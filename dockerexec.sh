#!/bin/sh

host=hostname
sabconf=
sickconf=
couchconf=
headconf=

downloadloc=
tvloc=
movieloc=
musicloc=

sabport=8080
sabssl=9090
sickport=8081
couchport=5050
headport=8181

if [ ! -d "$sabconf" ]; then
   mkdir $sabconf
fi

if [ ! -d "$sickconf" ]; then
   mkdir $sickconf
fi

if [ ! -d "$couchconf" ]; then
   mkdir $couchconf
fi
if [ ! -d "$headconf" ]; then
   mkdir $headconf
fi

docker build -t downloaders .

docker run -d -h $host -v $sabconf:/sabconfig -v $sickconf:/sickconfig -v $couchconf:/couchconfig -v $headconf:/headconf -v $downloadloc:/downloads -v $tvloc:/tv -v $movieloc:/movies -v $musicloc:/music -p $couchport:5050  -p $sabport:8080 -p $sickport:8081 -p $sabssl:9090 -p $headport:8181 downloaders
