# docker-downloaders #

This can be used to run the following in a single container:

*  sabnzbdplus

*  sickbeard

*  Couchpotato

To use it, modify dockerexec.sh for the relevant folders in your build then run the following.

```
#!bash

chmod +x dockerexec.sh
./dockerexec.sh
```