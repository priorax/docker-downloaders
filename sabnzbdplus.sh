
echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections        
echo "deb http://archive.ubuntu.com/ubuntu trusty multiverse" >> /etc/apt/sources.list
apt-get update                                                                        
apt-get install -qy python-software-properties software-properties-common screen      

add-apt-repository -y  ppa:jcfp/ppa

apt-get -q update
apt-get install -qy --force-yes  python git-core python-cheetah wget tar ca-certificates curl
apt-get install -qy --force-yes sabnzbdplus
apt-get install -qy --force-yes sabnzbdplus-theme-classic sabnzbdplus-theme-mobile sabnzbdplus-theme-plush
apt-get install -qy --force-yes par2 python-yenc unzip unrar
