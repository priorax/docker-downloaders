FROM ubuntu:14.04
MAINTAINER Doug Ferris <doug@dougferris.id.au>

VOLUME /movies
VOLUME /tv
VOLUME /downloads
VOLUME /music

VOLUME /couchconfig
VOLUME /sickconfig
VOLUME /sabconfig
VOLUME /headconf

ADD sabnzbdplus.sh /sabnzbdplus.sh
RUN chmod +x /sabnzbdplus.sh
RUN /sabnzbdplus.sh

ADD gitclone.sh /gitclone.sh
RUN chmod +x /gitclone.sh
RUN /gitclone.sh

ADD start.sh /start.sh
RUN chmod u+x  /start.sh

EXPOSE 5050 8080 8081 8181 9090

RUN apt-get autoremove &&\
    apt-get clean &&\
    rm -rf /var/lib/apt/lists/* &&\
    rm -rf /tmp/*
 
CMD ["/start.sh"]
