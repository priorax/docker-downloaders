#! /bin/sh

cd /CouchPotatoServer
touch /couchconfig/CouchPotato.cfg

screen -dmS couchpotato /usr/bin/python /CouchPotatoServer/CouchPotato.py --data_dir /movies/ --config_file /couchconfig/CouchPotato.cfg --console_log

screen -dmS sab /usr/bin/sabnzbdplus --daemon --config-file /sabconfig --server :8080 

screen -dmS python /headphones/Headphones.py --datadir /headconf

sleep 5

cd /sickbeard
if [ -f /sickconfig/config.ini ]
then
	rm -rf /sickbeard/config.ini
	rm -rf /sickbeard/sickbeard.db
#	rm -rf /sickbeard/autoprocesstv/autoProcessTV.cfg
else
	mv -f /sickbeard/config.ini /sickconfig/config.ini
	mv -f /sickbeard/sickbeard.db /sickconfig/sickbeard.db
#	mv -f /sickbeard/autoprocesstv/autoProcessTV.cfg /config/autoProcessTV.cfg
fi
ln -sf /sickconfig/config.ini /sickbeard/
ln -sf /sickconfig/sickbeard.db /sickbeard/sickbeard.db
#ln -sf /config/autoProcessTV.cfg sickbeard/autoprocesstv/autoProcessTV.cfg
/usr/bin/python SickBeard.py
